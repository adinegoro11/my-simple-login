<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_detail_table extends CI_Migration
{
    private $table = 'details';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => true,
                'auto_increment' => true
            ),
            'brand_id' => array(
                'type' => 'INT',
                'constraint' => 10,
                'null' => true,
            ),
            'vehicle_id' => array(
                'type' => 'INT',
                'constraint' => 10,
                'null' => true,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 250,
                'null' => true,
            ),
            'last_update' => array(
                'type' => 'DATETIME',
                'null' => true,
            ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->table, true);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table);
    }
}
