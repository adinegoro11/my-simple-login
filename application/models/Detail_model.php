<?php  if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Detail_model extends CI_Model
{
    private $table = 'details';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_condition($where = [])
    {
        $this->db->select('v.name jenis, b.name brand, d.name type');
        $this->db->from($this->table. ' d');
        $this->db->join('brands b', 'b.id = d.brand_id', 'left');
        $this->db->join('vehicles v', 'v.id = d.vehicle_id', 'left');
        foreach ($where as $key => $value) {
            $this->db->like($key, $value);
        }
        $this->db->order_by('jenis,brand,type');
        return $this->db->get()->result_array();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = 0, $data = [])
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
    }
}
