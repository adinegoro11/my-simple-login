<?php
defined('BASEPATH') or exit('No direct script access allowed');
include "./vendor/autoload.php";
class Welcome extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        try {
            $client_id = '781409915703-o9cck0lqch19u9vt2osj1ii1051lp5rp.apps.googleusercontent.com';
            $client_secret = 'M0V5U298i132IlWGLwGY5pjY';
            $redirect_url = base_url().'login/tes';
            $redirect_url =  'http://localhost/simple-login/';

            $client = new Google_Client();
            $client->setClientId($client_id);
            $client->setClientSecret($client_secret);
            $client->setRedirectUri($redirect_url);
            $client->addScope('profile');
            $client->addScope('email');
            $data['url_google'] = $client->createAuthUrl();

            $code = $this->input->get('code');
            if (isset($code)) {
                $token = $client->fetchAccessTokenWithAuthCode($code);
                $client->setAccessToken($token);
                $gauth = new Google_Service_Oauth2($client);
                $google_info = $gauth->userinfo->get();

                $data = [
                    'name' => $google_info->name,
                    'email' => $google_info->email,
                    'url_picture' => $google_info->picture,
                ];
                $this->load->view('v_success', $data);
            } else {
                $this->load->view('v_login', $data);
            }
        } catch (Exception $e) {
            $response = [
                'status' => 500,
                'error_message' => $e->getMessage(),
            ];

            $this->output
            ->set_status_header(500)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
            exit;
        }
    }
}
