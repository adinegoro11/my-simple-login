<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Vehicle extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        echo '<pre>';
        $input = $this->input->get('search');
        $search = [];
        if (isset($input)) {
            $where = explode("/", $input);
            // print_r($where);
            $search = [
                'v.name' => $where[0],
                'b.name' => $where[1] ?? false,
                'd.name' => $where[2] ?? false,
            ];
        }
        $query = $this->detail_model->get_by_condition($search);
        // print_r($query);

        $data = [];
        foreach ($query as $key => $value) {
            if (! in_array($value['jenis'], $data)) {
                $data[] = $value['jenis'];
            }
            if (! in_array($value['jenis']."/".$value['brand'], $data)) {
                $data[] = $value['jenis']."/".$value['brand'];
            }
            if (! in_array($value['jenis']."/".$value['brand']."/".$value['type'], $data)) {
                $data[] = $value['jenis']."/".$value['brand']."/".$value['type'];
            }
        }
        print_r($data);
        die();
    }
}
